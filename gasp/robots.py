from gasp import *

begin_graphics()
finished = False



def place_player():
    global player_x, player_y, player_shape, place_player
    player_x = random_between(0,63)
    player_y = random_between(0,47)
    player_shape = Circle((player_x, player_y), 5)

def move_player():
    global player_x, player_y, player_shape, move_player
    
    sleep(0.05)
    
    key = update_when('key_pressed')
    
    #player right
    if key == 'd' and player_x > 0 and player_x < 63:
        player_x += 1
    
    #player diagonally down right
    elif key == 'x':
        if player_x > 0 and player_x < 63:
            player_x += 1
        if player_y > 0 and player_y < 47:
            player_y -= 1
   
   #player left 
    elif key == 'a' and player_x > 0 and player_x < 63:
       player_x -= 1
    #diagonally down left
    elif key == 'z':
        if player_x > 0 and player_x < 63:
            player_x -= 1
        if player_y > 0 and player_y < 47: 
            player_y -=1
    
    #player up
    elif key == 'w' and player_y < 47 and player_y > 0:
        player_y += 1

    #player down
    elif key == 's' and player_y < 47 and player_y > 0:
        player_y -= 1
    
    #player up right 
    elif key == 'e':
        if player_x > 0 and player_x < 63:
            player_x += 1
        if player_y > 0 and player_y < 47:
            player_y += 1

    #player up left 
    elif key == 'q':
        if player_x > 0 and player_x < 63:
            player_x -= 1
        if player_y > 0 and player_y < 47:
            player_y += 1
    
    move_to(player_shape, (10*player_x+5, 10*player_y+5))
    
def place_robot():
    global robot_x, robot_y, robot_shape
    robot_x = random_between(0, 63)
    robot_y = random_between(0, 47)
    robot_shape = Box((10*robot_x, 10*robot_y), 8, 8)
def move_robot():
    global robot_x, robot_y, robot_shape
    
    #player is right of robot
    if player_x > robot_x:
        if player_y > robot_y:
            robot_y += 1
            robot_x += 1
        elif player_y < robot_y:
            robot_y -= 1
            robot_x += 1
        elif player_y == robot_y:
            robot_x += 1

    #player is left of robot
    elif player_x < robot_x:
        if player_y > robot_y:
            robot_y += 1
            robot_x -= 1
        elif player_y < robot_y:
            robot_y -= 1
            robot_x -= 1
        elif player_y == robot_y:
            robot_x -= 1

    elif player_x == robot_x:
        if player_y > robot_y:
            robot_y += 1
        elif player_y < robot_y:
            robot_y -= 1
    move_to(robot_shape, (10 * robot_x, 10 * robot_y))

def check_collisions():
    global robot_x, robot_y, player_x, player_y
    if player_x == robot_x and player_y == robot_y:
        finished = True
        print("You've Been Caught! 0-0")
place_player()
place_robot()
check_collisions()
while not finished:
    move_player()

end_graphics()
