import numpy as np
#1
def to_binary(num):
    """
    Convert an decimal integer into a string representation of its binary
    (base2) digits.

      >>> to_binary(10) 
      '1010'
      >>> to_binary(12) 
      '1100'
      >>> to_binary(0) 
      '0'
      >>> to_binary(1) 
      '1'
    """
    return to_base(num, 2)

#2 
def to_base3(num):
    """
    Convert an decimal integer into a string representation of its base3
    digits.

      >>> to_base3(10)
      '101'
      >>> to_base3(12)
      '110'
      >>> to_base3(6)
      '20'
    """
    return np.base_repr(num,base=3)

# 3 
def to_base4(num):
    """
    Convert an decimal integer into a string representation of its base4
    digits.

      >>> to_base4(20)
      '110'
      >>> to_base4(28)
      '130'
      >>> to_base4(3)
      '3'
    """
    return np.base_repr(num,base=4)

# 4
def to_base(num, base):
    """
    Convert an decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 10) provided.

      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
      >>> to_base(21, 3)
      '210'
      >>> to_base(0, 2)
      '0'
      >>> to_base(21, 11)
      '1A'
      >>> to_base(47, 16)
      '2F'
      >>> to_base(65535, 16)
      'FFFF'
      >>> to_base(14, 64)
      'O'
      >>> to_base(63, 64)
      '/'
    """
    if num == 0:
        return '0'
    
    numstr = ''

    digits = '0123456789ABCDEFG'
    base64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
   
    if base == 64:
        digits = base64
    
    while num:
        numstr = digits[num % base] + numstr
        num //= base
    
    return numstr

if __name__ == '__main__':
    import doctest
    doctest.testmod()
