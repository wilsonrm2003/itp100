# 1 
def only_evens(nums): # define the function
    """
      >>> only_evens([3, 8, 5, 4, 12, 7, 2])
      [8, 4, 12, 2]
      >>> my_nums = [4, 7, 19, 22, 42]
      >>> only_evens(my_nums)
      [4, 22, 42]
      >>> my_nums
      [4, 7, 19, 22, 42]
    """
    for i in range(len(nums)): #make the function check each number 
        if nums[i] % 2 == 0: # is even
            only_evens += [nums[i]] # add any even numbers back into the function 
    return only_evens # return the even list

# 2
def num_even_digits(num): # define the function
    """
      >>> num_even_digits(123456)
      3
      >>> num_even_digits(2468)
      4
      >>> num_even_digits(1357)
      0
      >>> num_even_digits(2)
      1
      >>> num_even_digits(20)
      2
    """
    total_num = 0 # make a total
    stringN = str(num) # make the number into a string so I can split it 
    nList = [] # make a place for the split numbers to go 
    for i in range(len(stringN)): # check each value in stringN
        split = stringN[i:i + 1] # split the string 
        split = int(split) # make each value an integer 
        nList.append(split) # add each number to nList
    for i in range(len(nList)): # check each number in nList
        if nList[i] % 2 == 0: # check if even 
            total_num += 1 # if even, add one to total number of evens
    return total_num # return the total number of even numbers

# 3 
def sum_of_squares_of_digits(n): # define function
    """
      >>> sum_of_squares_of_digits(1)
      1
      >>> sum_of_squares_of_digits(9)
      81
      >>> sum_of_squares_of_digits(11)
      2
      >>> sum_of_squares_oif_digits(121)
      6
      >>> sum_of_squares_of_digits(987)
      194
    """
    total = 0 # make a total so the value can go there
    nString = str(n) # make number into string for easier split
    nList = [] # make a list for the split numbers to go
    for i in range(len(nString)): # check each number in the nString
        split = nString[i:i + 1] # split the n string 
        split = int(split) # make the string split into an int
        nList.append(split) # put the int into the nList 
    for i in range(len(nList)): # check each number in the nList 
        total += nList[i] * nList[i] # square the numbers in n list and add them
    return total # return total  

# 4

# 5 

# 6 

if __name__ == '__main__':
    import doctest
    doctest.testmod()
